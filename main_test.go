package main

import (
	"math/rand"
	"testing"
)

func TestLevenshteinMatrix(t *testing.T) {
	dist, _ := LevenshteinMatrix("skat", "kot")
	if dist != 2 {
		t.Fatal("Invalid LevenshteinMatrix distance for example from class lesson")
	}
	dist, _ = LevenshteinMatrix("transpos", "rtanspos")
	if dist != 2 {
		t.Fatal("Invalid LevenshteinMatrix distance with transposition, need 2")
	}
	dist, _ = LevenshteinMatrix("123456", "")
	if dist != 6 {
		t.Fatal("Invalid LevenshteinMatrix distance with transposition")
	}
	dist, _ = LevenshteinMatrix("", "123456")
	if dist != 6 {
		t.Fatal("Invalid LevenshteinMatrix distance with transposition")
	}
}

func TestDamerauLevenshteinMatrix(t *testing.T) {
	dist, _ := DamerauLevenshteinMatrix("skat", "kot")
	if dist != 2 {
		t.Fatal("Invalid DamerauLevenshteinMatrix distance for example from class lesson")
	}
	dist, _ = DamerauLevenshteinMatrix("transpos", "rtanspos")
	if dist != 1 {
		t.Fatal("Invalid DamerauLevenshteinMatrix distance with transposition, need 1")
	}
	dist, _ = DamerauLevenshteinMatrix("123456", "")
	if dist != 6 {
		t.Fatal()
	}
	dist, _ = DamerauLevenshteinMatrix("", "123456")
	if dist != 6 {
		t.Fatal()
	}
}

func TestLevenshteinRecursive(t *testing.T) {
	if LevenshteinRecursive("abc", "abc") != 0 {
		t.Fatal()
	}
	if LevenshteinRecursive("abc", "adc") != 1 {
		t.Fatal()
	}
	if LevenshteinRecursive("skat", "kot") != 2 {
		t.Fatal()
	}
}

func TestLevenshteinRecursiveCached(t *testing.T) {
	if LevenshteinRecursiveCachedMap("abc", "abc") != 0 {
		t.Fatal()
	}
	if LevenshteinRecursiveCachedMap("abc", "adc") != 1 {
		t.Fatal()
	}
	if LevenshteinRecursiveCachedMap("skat", "kot") != 2 {
		t.Fatal()
	}
}

func TestCalculateDiagonalScore(t *testing.T) {
	s1 := "ab"
	s2 := "az"
	res := calculateDiagonalScore(s1[:1], s2[:1])
	if res != 0 {
		t.Fatal("Invalid diagonal score, need to be zero")
	}
	res = calculateDiagonalScore(s1[1:2], s2[1:2])
	if res == 0 {
		t.Fatal("Invalid diagonal score, need to be one")
	}

}

func TestMain(t *testing.T) {
	main()
}

//---------------------------------------------------------------------------------------------

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randomString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func benchmrkLevenshteinMatrix(length int, b *testing.B) {
	s1, s2 := randomString(length), randomString(length)
	for n := 0; n < b.N; n++ {
		LevenshteinMatrix(s1, s2)
	}
}
func BenchmarkLevenshteinMatrix10(b *testing.B) {
	benchmrkLevenshteinMatrix(10, b)
}
func BenchmarkLevenshteinMatrix20(b *testing.B) {
	benchmrkLevenshteinMatrix(20, b)
}
func BenchmarkLevenshteinMatrix30(b *testing.B) {
	benchmrkLevenshteinMatrix(30, b)
}
func BenchmarkLevenshteinMatrix40(b *testing.B) {
	benchmrkLevenshteinMatrix(40, b)
}
func BenchmarkLevenshteinMatrix50(b *testing.B) {
	benchmrkLevenshteinMatrix(50, b)
}
func benchmrkDamerauLevenshteinMatrix(length int, b *testing.B) {
	s1, s2 := randomString(length), randomString(length)
	for n := 0; n < b.N; n++ {
		DamerauLevenshteinMatrix(s1, s2)
	}
}
func BenchmarkDamerauLevenshteinMatrix10(b *testing.B) {
	benchmrkDamerauLevenshteinMatrix(10, b)
}
func BenchmarkDamerauLevenshteinMatrix20(b *testing.B) {
	benchmrkDamerauLevenshteinMatrix(20, b)
}
func BenchmarkDamerauLevenshteinMatrix30(b *testing.B) {
	benchmrkDamerauLevenshteinMatrix(30, b)
}
func BenchmarkDamerauLevenshteinMatrix40(b *testing.B) {
	benchmrkDamerauLevenshteinMatrix(40, b)
}
func BenchmarkDamerauLevenshteinMatrix50(b *testing.B) {
	benchmrkDamerauLevenshteinMatrix(50, b)
}
func benchmrkLevenshteinRecursive(length int, b *testing.B) {
	s1, s2 := randomString(length), randomString(length)
	for n := 0; n < b.N; n++ {
		LevenshteinRecursive(s1, s2)
	}
}
func BenchmarkLevenshteinRecursive10(b *testing.B) {
	benchmrkLevenshteinRecursive(10, b)
}
func BenchmarkLevenshteinRecursive11(b *testing.B) {
	benchmrkLevenshteinRecursive(11, b)
}
func BenchmarkLevenshteinRecursive12(b *testing.B) {
	benchmrkLevenshteinRecursive(12, b)
}
func benchmrkLevenshteinRecursiveCached(length int, b *testing.B) {
	s1, s2 := randomString(length), randomString(length)
	for n := 0; n < b.N; n++ {
		LevenshteinRecursiveCachedMap(s1, s2)
	}
}
func BenchmarkLevenshteinRecursiveCached10(b *testing.B) {
	benchmrkLevenshteinRecursiveCached(10, b)
}
func BenchmarkLevenshteinRecursiveCached20(b *testing.B) {
	benchmrkLevenshteinRecursiveCached(20, b)
}
func BenchmarkLevenshteinRecursiveCached30(b *testing.B) {
	benchmrkLevenshteinRecursiveCached(30, b)
}
func BenchmarkLevenshteinRecursiveCached40(b *testing.B) {
	benchmrkLevenshteinRecursiveCached(40, b)
}
func BenchmarkLevenshteinRecursiveCached50(b *testing.B) {
	benchmrkLevenshteinRecursiveCached(50, b)
}