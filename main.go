package main

import (
	"fmt"
)

func main() {
	dist := LevenshteinRecursiveCachedMap("skat", "kot")
	fmt.Println(dist)
}

type matrix struct {
	a [][]int
}

func createMatrix(x, y int) matrix {
	// a00 всегда = 0, поэтому добавляем плюс один ряд и столбец
	a := make([][]int, x+1)
	for i := range a {
		a[i] = make([]int, y+1)
	}
	return matrix{a}
}

func createMatrixS(s1, s2 string) matrix {
	return createMatrix(len(s1), len(s2))
}

func (m matrix) String() string {
	var buf string
	for _, elem := range m.a {
		buf += fmt.Sprintf("%v\n", elem)
	}
	return string(buf)

}

func populateTrivialRow(m *matrix, s string) {
	trivalRow := (*m).a[0]
	for i := 1; i < len(trivalRow); i++ {
		trivalRow[i] = i
	}
}

func populateTrivialColumn(m *matrix, s string) {
	for i := 1; i <= len(s); i++ {
		(*m).a[i][0] = i
	}
}

func calculateDiagonalScore(l1, l2 string) int {
	if l1 == l2 {
		return 0
	}
	return 1
}

func min(nums ... int) int {
    min := nums[0]
    for _, num := range nums {
        if (num < min) {
			min = num
		}
    }
    return min
}

func LevenshteinMatrix(s1, s2 string) (distance int, matrix matrix) {
	// нужно создать 2д массив, далее заполняем тривиальные строку столбец
	// потом идем построчно и высчитываем через минималку
	m := createMatrixS(s1, s2)
	populateTrivialColumn(&m, s1)
	populateTrivialRow(&m, s2)

	for i := 1; i <= len(s1); i++ {
		for j := 1; j <= len(s2); j++ {
			hor_move := m.a[i][j-1] + 1
			vert_move := m.a[i-1][j] + 1
			diag_move := m.a[i-1][j-1] + calculateDiagonalScore(s1[i-1:i], s2[j-1:j])
			result_score := min(diag_move, min(hor_move, vert_move))
			m.a[i][j] = result_score
		}
	}
	//fmt.Print(m)
	return m.a[len(s1)][len(s2)], m
}

func DamerauLevenshteinMatrix(s1, s2 string) (distance int, matrix matrix) {

	m := createMatrixS(s1, s2)
	populateTrivialColumn(&m, s1)
	populateTrivialRow(&m, s2)

	for i := 1; i <= len(s1); i++ {
		for j := 1; j <= len(s2); j++ {
			hor_move := m.a[i][j-1] + 1
			vert_move := m.a[i-1][j] + 1
			diag_move := m.a[i-1][j-1] + calculateDiagonalScore(s1[i-1:i], s2[j-1:j])
			result_score := min(diag_move, min(hor_move, vert_move))

			if i > 1 && j > 1 && s1[i-1:i] == s2[j-2:j-1] && s1[i-2:i-1] == s2[j-1:j] {
				result_score = min(result_score, m.a[i-2][j-2]+1)
			}
			m.a[i][j] = result_score
		}
	}
	//fmt.Print(m)
	return m.a[len(s1)][len(s2)], m
}

func LevenshteinRecursive(s1, s2 string) (distance int) {
	//В рекурсии всегда откусываем справа: 3 вызова для (кот, скат) есть откусывание у кот, есть откусывание у обоих, есть откусывание у скат. 
	if len(s1) == 0 {
		return len(s2)
	}
	if len(s2) == 0 {
		return len(s1)
	}
	// надо проверить последний символ который откусываем, если одинаковый то +0
	bothRemoveScore := 1
	if s1[len(s1)-1:] == s2[len(s2)-1:] {
		bothRemoveScore = 0
	}
	return min(
		LevenshteinRecursive(s1[:len(s1)-1], s2) + 1,
		LevenshteinRecursive(s1[:len(s1)-1], s2[:len(s2)-1]) + bothRemoveScore,
		LevenshteinRecursive(s1, s2[:len(s2)-1]) + 1,
	)
}

func LevenshteinRecursiveCachedMap(s1, s2 string) (distance int) {
	cache := make(map[string]int)
	return levenshteinRecursiveCached(s1, s2, &cache)
}

func levenshteinRecursiveCached(s1, s2 string, cache *map[string]int) (distance int) {
	key := s1 + "~" + s2
	elem, present := (*cache)[key]
	if present {
		return elem
	}
	if len(s1) == 0 {
		(*cache)[key] = len(s2)
		return len(s2)
	}
	if len(s2) == 0 {
		(*cache)[key] = len(s1)
		return len(s1)
	}
	bothRemoveScore := 1
	if s1[len(s1)-1:] == s2[len(s2)-1:] {
		bothRemoveScore = 0
	}
	res := min(
		levenshteinRecursiveCached(s1[:len(s1)-1], s2, cache) + 1,
		levenshteinRecursiveCached(s1[:len(s1)-1], s2[:len(s2)-1], cache) + bothRemoveScore,
		levenshteinRecursiveCached(s1, s2[:len(s2)-1], cache) + 1,
	)
	(*cache)[key] = res
	return res
}